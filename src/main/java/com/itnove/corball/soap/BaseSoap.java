package com.itnove.corball.soap;

import com.itnove.corball.drivers.GenericDriver;
import com.itnove.corball.utils.PropertiesUtils;
import org.apache.logging.log4j.LogManager;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.lang.reflect.Method;
import java.util.Properties;

/**
 * Created by guillem.hernandez on 12/12/2014.
 */

public class BaseSoap extends GenericDriver {
    public static final Properties testProperties =
            PropertiesUtils.getProcessedTestProperties();
    public static final String endPoint = testProperties.getProperty(PropertiesUtils.HOST);
    public static final String user = testProperties.getProperty(PropertiesUtils.USER);
    public static final String passwd = testProperties.getProperty(PropertiesUtils.PASSWD);

    @BeforeMethod(alwaysRun = true)
    public void setUp(Method method) {
        globalSoapDriver.set(new SOAPClient());
        globalLogger.set(LogManager.getLogger(BaseSoap.class));
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(Method method, ITestResult result) {
        globalLogger.get().info("Finished execution for testcase ");
        try {
            if (result.getStatus() == ITestResult.SUCCESS) {
                globalLogger.get().info("Finishing " + method.getName() + " Test Passed");
            } else if (result.getStatus() == ITestResult.FAILURE) {
                globalLogger.get().info("Finishing " + method.getName() + " Test Failed");
            } else if (result.getStatus() == ITestResult.SKIP) {
                globalLogger.get().info("Finishing " + method.getName() + " Test Skipped");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
