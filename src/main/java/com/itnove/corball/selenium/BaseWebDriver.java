package com.itnove.corball.selenium;

import com.itnove.corball.drivers.GenericSauceDriver;
import com.itnove.corball.utils.PropertiesUtils;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.IOException;
import java.lang.reflect.Method;

/**
 * Base Selenium Class to execute all the tests
 *
 * @author guillem
 */
public class BaseWebDriver extends GenericSauceDriver {

    public static final int DRIVER_SELENIUM_TIMEOUT_SECONDS = 120;
    public static final String browserName =
            testProperties.getProperty(PropertiesUtils.BROWSER);
    public static final String user = testProperties.getProperty(PropertiesUtils.USER);
    public static final String passwd = testProperties.getProperty(PropertiesUtils.PASSWD);
    public static final String startUrl = testProperties.getProperty(PropertiesUtils.HOST);
    public static RemoteWebDriverWait wait;

    @BeforeMethod(alwaysRun = true)
    public void setUp(Method method, Object[] testArguments)
            throws IOException {
        StartWebDriver startWebDriver = new StartWebDriver();
        startWebDriver.startWebDriver(globalLogger,
                globalDriver,
                globalBrowserCapabilities,
                globalJse,
                sessionId,
                method, hub, browserName,
                DRIVER_SELENIUM_TIMEOUT_SECONDS, host);
        wait = new RemoteWebDriverWait(globalDriver.get(), DRIVER_SELENIUM_TIMEOUT_SECONDS);
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(Method method, ITestResult result) {
        globalDriver.get().quit();
        try {
            if (result.getStatus() == ITestResult.SUCCESS) {
                globalLogger.get().info("Finishing " + method.getName() + " Test Passed");
            } else if (result.getStatus() == ITestResult.FAILURE) {
                globalLogger.get().info("Finishing " + method.getName() + " Test Failed");
            } else if (result.getStatus() == ITestResult.SKIP) {
                globalLogger.get().info("Finishing " + method.getName() + " Test Skipped");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
