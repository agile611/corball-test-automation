Corball Functional Testing Framework
===============

Corball test automation packages, a functional test automation framework in Java and based on TestNG

Corball Supported Platform
==========================

Corball supports automation on the following platforms:

* Selenium WebDriver
* Native Android application
* Native IOS application
* Chrome Mobile Browser
* Safari Mobile Browser
* XML and SOAP based applications
* Appium Desktop apps 

Corball Functional Testing Framework Maven Dependency
===============

To start using Corball, just add this dependency on your pom.xml from your project

    <dependency>
        <groupId>com.itnove.corball</groupId>
        <artifactId>corball-core</artifactId>
        <version>17.05.12</version>
    </dependency>

Check historical versions here

[All previous SNAPSHOTS](https://oss.sonatype.org/content/repositories/snapshots/com/itnove/corball)
[All previous RELEASES](https://oss.sonatype.org/content/repositories/snapshots/com/itnove/corball)

Corball Functional Testing Framework DEMO package
===============

Here you can find the example tests demo, you have to checkout the code from here:

[Corball Test Automation Demo](https://bitbucket.org/itnove/corball-test-automation-demo)

You will find examples for the following platforms:

* Selenium WebDriver
* Native Android application
* Native IOS application
* Chrome Mobile Browser
* Safari Mobile Browser
* XML and SOAP based applications

## Support

This tutorial is released into the public domain by [Guillem Hernández Sola](https://www.linkedin.com/in/guillemhernandezsola/)  under WTFPL.

[![WTFPL](http://www.wtfpl.net/wp-content/uploads/2012/12/wtfpl-badge-1.png)](http://www.wtfpl.net/)

This README file was originally written by [Guillem Hernández Sola](https://www.linkedin.com/in/guillemhernandezsola/) and is likewise released into the public domain.

Please contact [Guillem Hernández Sola](https://www.linkedin.com/in/guillemhernandezsola/) for further details.
